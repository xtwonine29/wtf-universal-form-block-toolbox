var rowGaps = [
    "f-rg-tiny",
    "f-rg-small",
    "f-rg-normal",
    "f-rg-large",
];

var rowHeight = [
    "f-rh-tiny",
    "f-rh-small",
    "f-rh-normal",
    "f-rh-large",
];

var borderRadius = [
    "f-br-none",
    "f-br-bottom",
    "f-br-all",
];

var formVariant = [
    "b-form-default",
    "b-form-light",
];

var applyStyles = function(styleData){

    setTimeout(function(){

        $("body").removeClass(rowGaps.join(" ")).addClass(styleData.rowGaps);
        $("body").removeClass(borderRadius.join(" ")).addClass(styleData.borderRadius);
        $("body").removeClass(rowHeight.join(" ")).addClass(styleData.rowHeight);
        $("body").removeClass(formVariant.join(" ")).addClass(styleData.formVariant);

        $("body").addClass("helpers-loaded");


    }, 200);

};

chrome.runtime.onMessage.addListener(
    function(message, sender, sendResponse) {
        switch(message.type) {
            case "applyStyles":
                applyStyles(message.styleData);
                sendResponse("Done applying styles");
                break;
        }
    }
);

$(document).ready(function(){
    chrome.storage.sync.get(['wtf-styles-data', 'wtf-auto-apply'], function (items) {
        if(typeof items['wtf-auto-apply'] !== 'undefined') {
            applyStyles(items['wtf-styles-data']);
        }
    })
});

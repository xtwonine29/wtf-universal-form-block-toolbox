$(document).ready(function(){

    $("#status").html("Hello");

    // setting the saved/default values when popup is opened

    chrome.storage.sync.get(['wtf-styles-data', 'wtf-auto-apply'], function(items){
        // console.log("Saved: " + items['wtf-styles-data']);
        // console.log("Autopaint: " + items['wtf-auto-apply']);

        if(typeof items['wtf-styles-data'] !== 'undefined'){
            $('#row-gaps').val(items['wtf-styles-data'].rowGaps);
            $('#row-height').val(items['wtf-styles-data'].rowHeight);
            $('#border-radius').val(items['wtf-styles-data'].borderRadius);
            $('#form-variant').val(items['wtf-styles-data'].formVariant);
        }

        if(typeof items['wtf-auto-apply'] !== 'undefined'){
            $("#styles-autoapply").attr("checked", true);
        }
    });

    // binding everything

    $("#row-gaps, #row-height, #border-radius, #form-variant").on("change", function(e){
        console.log("change");
        chrome.storage.sync.set(
            {'wtf-styles-data': {
                rowGaps: $("#row-gaps").val(),
                rowHeight: $("#row-height").val(),
                borderRadius: $("#border-radius").val(),
                formVariant: $("#form-variant").val(),
            }
        }, function() {
            $("#status").html('Class config saved');
            console.log("Saved: " + {
                rowGaps: $("#row-gaps").val(),
                rowHeight: $("#row-height").val(),
                borderRadius: $("#border-radius").val(),
                formVariant: $("#form-variant").val(),
            });

        });
    });

    $("#styles-autoapply").on("click", function(e){
                console.log("Autoapply state: " + ($("#styles-autoapply").is(":checked")));
        if($("#styles-autoapply").is(":checked")){
            chrome.storage.sync.set({'wtf-auto-apply': true}, function() {
                $("#status").html('Autoapply engaged');
            });
        }else{
            chrome.storage.sync.remove('wtf-auto-apply', function() {
                $("#status").html('Autoapply disengaged');
            });
        }
    });

    $("#styles-submit").on("click", function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        $("#status").html("Working...");

        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            chrome.tabs.sendMessage(tabs[0].id, {
                type:"applyStyles",
                styleData: {
                    rowGaps: $("#row-gaps").val(),
                    rowHeight: $("#row-height").val(),
                    borderRadius: $("#border-radius").val(),
                    formVariant: $("#form-variant").val(),
                }
            }, function(response) {
                $("#status").html(response);
            })
        })
    })
});
